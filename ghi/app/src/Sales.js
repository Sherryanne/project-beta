import React, { useEffect, useState } from 'react';

function Sales () {
    const [sales, setSales] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('http://localhost:8090/api/sales/');
                if (response.ok) {
                    const info = await response.json();
                    setSales(info.sales);
                }
            } catch (error) {
                console.error("Error fetching data:", error);
            }
        };

        fetchData();
    }, []);

return(
    <><h1>Sales</h1>
    <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
                <tbody>
                    {sales.map(sale => {
                        return(
                            <tr key={sale.id}>
                                <td>{sale.sales_person.id}</td>
                                <td>{`${sale.sales_person.first_name} ${sale.sales_person.last_name}`}</td>
                                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.sale_price}</td>
                        </tr>
                            )
                        })}
                </tbody>
    </table>
</>
);





































}
export default Sales;
