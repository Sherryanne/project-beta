import React, { useState, useEffect } from 'react';

function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    async function fetchTechnicians() {
      const response = await fetch('http://localhost:8080/api/technicians/');
      const data = await response.json();
      setTechnicians(data.technicians);
    }

    fetchTechnicians();
  }, []);

  return (
    <div>
      <h1>Technicians</h1>
      <table className="table">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((tech) => (
            <tr key={tech.employee_id}>
              <td>{tech.employee_id}</td>
              <td>{tech.first_name}</td>
              <td>{tech.last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default TechnicianList;
