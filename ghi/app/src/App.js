import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from "./ManufacturerList";
import AddTechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import CreateAppointmentForm from './CreateAppointmentForm';
import ServiceAppointments from './ServiceAppointment';
import ServiceHistoryForm from './ServiceHistoryForm';
import CreateManufacturerForm from './ManufacturereForm';
import VehicleModelList from './VehicleModel';
import CreateVehicleModel from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import CreateAutomobileForm from './AutomobileForm';
import Salespeople from './Salespeople'
import CreateSalesPerson from './CreateSalesPerson'
import Customers from './Customers'
import CreateCustomer from './CreateCustomer'
import Sales from './Sales'
import CreateSale from './CreateSale'
import SalesPersonHistory from './SalesPersonHistory'


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/" element={<ManufacturerList/>} />
          <Route path="technician/create" element={<AddTechnicianForm/>} />
          <Route path="technicians/" element={<TechnicianList/>} />
          <Route path="Appointment/create" element={<CreateAppointmentForm/>} />
          <Route path="/appointments" element={<ServiceAppointments />} />
          <Route path="/service-history" element={<ServiceHistoryForm />} />
          <Route path="/manufacturers/create" element={<CreateManufacturerForm />} />
          <Route path="/models" element={<VehicleModelList />} />
          <Route path="/models/create" element={<CreateVehicleModel />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/automobiles/create" element={<CreateAutomobileForm />} />
          <Route path="/salespeople" element={<Salespeople />} />
          <Route path="/createsalesperson" element={<CreateSalesPerson />} />
          <Route path="/customers" element={<Customers />} />
          <Route path="/createcustomer" element={<CreateCustomer />} />
          <Route path="/sales" element={<Sales />} />
          <Route path="/createsale" element={<CreateSale />} />
          <Route path="/salespersonhistory" element={<SalesPersonHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

// export default App;
// import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import MainPage from './MainPage';
// import Nav from './Nav';
// import Salespeople from './Salespeople'
// import CreateSalesPerson from './CreateSalesPerson'
// import Customers from './Customers'
// import CreateCustomer from './CreateCustomer'
// import Sales from './Sales'
// import CreateSale from './CreateSale'
// import SalesPersonHistory from './SalesPersonHistory'
// import { useEffect, useState } from 'react';



// function App() {
//   return (
//     <BrowserRouter>
//       <Nav />
//       <div className="container">
//         <Routes>
//           {/* <Route path="/" element={<MainPage />} /> */}
//           <Route path="/salespeople" element={<Salespeople />} />
//           <Route path="/createsalesperson" element={<CreateSalesPerson />} />
//           <Route path="/customers" element={<Customers />} />
//           <Route path="/createcustomer" element={<CreateCustomer />} />
//           <Route path="/sales" element={<Sales />} />
//           <Route path="/createsale" element={<CreateSale />} />
//           <Route path="/salespersonhistory" element={<SalesPersonHistory />} />
//         </Routes>
//       </div>
//     </BrowserRouter>
//   );
// }
export default App;
