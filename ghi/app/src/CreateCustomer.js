import React, {useState} from 'react';

function CreateCustomer() {
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const[phone_number,setPhoneNumber] = useState('')

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value);
    }
    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value)
    }
    const handleAddressChange = (e) => {
        const value = e.target.value;
        setAddress(value);
    }
    const handlePhoneNumberChange = (e) => {
        const value = e.target.value;
        setPhoneNumber(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

    const data = {
        first_name,
        last_name,
        address,
        phone_number,
    }


const url = 'http://localhost:8090/api/customers/';
const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
        'Content-Type': 'application/json',
    }
}

const response = await fetch(url, fetchConfig)
if (response.ok) {
    const customer = await response.json()
    setFirstName('')
    setLastName('')
    setAddress('')
    setPhoneNumber('')
    }
}


// return (
//     <>
// <h1>Create a customer</h1>
// <form onSubmit={handleSubmit}>
//     <div>
//         <input onChange={handleFirstNameChange} value={first_name} name="firstName" placeholder="First name..." required type="text" id="first_name" className='form-control'/>
//         <label htmlFor="first_name"></label>
//     </div>
//     <div>
//         <input onChange={handleLastNameChange} value={last_name} name="lastName" placeholder="Last name..." required type="text" id="last_name" className='form-control'/>
//         <label htmlFor="last_name"></label>
//     </div>
//     <div>
//         <input onChange={handleAddressChange} value={address} name="address" placeholder="Address..." required type="text" id="address" className='form-control'/>
//         <label htmlFor="address"></label>
//     </div>
//     <div>
//         <input onChange={handlePhoneNumberChange} value={phone_number} name="phoneNumber" placeholder="Phone number..." required type="text" id="phone_number" className='form-control'/>
//         <label htmlFor="phone_number"></label>
//     </div>
//     <button className="btn btn-primary">Create</button>
// </form>
//     </>

// );
// }

// export default CreateCustomer;



return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a Customer</h1>
                <form onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input
                            onChange={handleFirstNameChange}
                            value={first_name}
                            name="firstName"
                            placeholder="First name"
                            required
                            type="text"
                            id="first_name"
                            className="form-control"
                        />
                        <label htmlFor="first_name">First name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            onChange={handleLastNameChange}
                            value={last_name}
                            name="lastName"
                            placeholder="Last name"
                            required
                            type="text"
                            id="last_name"
                            className="form-control"
                        />
                        <label htmlFor="last_name">Last name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            onChange={handleAddressChange}
                            value={address}
                            name="address"
                            placeholder="Address"
                            required
                            type="text"
                            id="address"
                            className="form-control"
                        />
                        <label htmlFor="address">Address</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            onChange={handlePhoneNumberChange}
                            value={phone_number}
                            name="phoneNumber"
                            placeholder="Phone number"
                            required
                            type="text"
                            id="phone_number"
                            className="form-control"
                        />
                        <label htmlFor="phone_number">Phone number</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
);
}

export default CreateCustomer;