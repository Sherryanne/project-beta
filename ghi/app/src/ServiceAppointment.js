import React, { useState, useEffect } from 'react';

function ServiceAppointments() {
  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    async function getAppointments() {
      try {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const data = await response.json();
        setAppointments(data.appointments);
      } catch (error) {
        console.error("Could not fetch appointments:", error);
      }
    }

    getAppointments();
  }, []);

  const updateAppointmentStatus = async (id, newStatus) => {
    try {
      const response = await fetch(`http://localhost:8080/api/appointments/${id}/${newStatus}/`, { method: 'PUT' });
      if (!response.ok) {
        throw new Error(`Failed to update the appointment status to ${newStatus}`);
      }
      const updatedAppointment = await response.json();
      setAppointments(prevAppointments =>
        prevAppointments.map(appointment =>
          appointment.id === id ? { ...appointment, status: updatedAppointment.status } : appointment
        )
      );
    } catch (error) {
      console.error(`Could not update appointment status to ${newStatus}:`, error);
    }
  };

  const formatDateTime = (dateTimeStr) => {
    const dateTime = new Date(dateTimeStr);
    const date = dateTime.toLocaleDateString();
    const time = dateTime.toLocaleTimeString();
    return { date, time };
  };

  return (
    <div>
      <h1>Service Appointments</h1>
      <table className="table table-white table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            const { date, time } = formatDateTime(appointment.date_time);
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.isVip ? 'Yes' : 'No'}</td>
                <td>{appointment.customer}</td>
                <td>{date}</td>
                <td>{time}</td>
                <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
                <td>
                    <button type="button" className="btn btn-danger me-2" onClick={() => updateAppointmentStatus(appointment.id, 'cancel')}>Cancel</button>
                    <button type="button" className="btn btn-success" onClick={() => updateAppointmentStatus(appointment.id, 'finish')}>Finish</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceAppointments;
