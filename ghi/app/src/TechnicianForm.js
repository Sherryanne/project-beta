import React, { useState } from 'react';

function AddTechnicianForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            employee_id: employeeId,
            first_name: firstName,
            last_name: lastName
        };

        try {
            const response = await fetch('http://localhost:8080/api/technicians/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            });

            if (!response.ok) {
                const errorDetail = await response.text(); 
                throw new Error(`HTTP error! status: ${response.status}, detail: ${errorDetail}`);
            }

            const responseData = await response.json();
            console.log('Response:', responseData);
        } catch (error) {
            console.error('There was an error!', error);
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                type="text"
                                className="form-control"
                                id="firstName"
                                value={firstName}
                                onChange={e => setFirstName(e.target.value)}
                                placeholder="First name"
                            />
                            <label htmlFor="firstName">First name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                type="text"
                                className="form-control"
                                id="lastName"
                                value={lastName}
                                onChange={e => setLastName(e.target.value)}
                                placeholder="Last name"
                            />
                            <label htmlFor="lastName">Last name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                type="text"
                                className="form-control"
                                id="employeeId"
                                value={employeeId}
                                onChange={e => setEmployeeId(e.target.value)}
                                placeholder="Employee ID"
                            />
                            <label htmlFor="employeeId">Employee ID</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AddTechnicianForm;
