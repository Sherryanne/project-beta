import React, { useState, useEffect } from 'react';


function CreateAppointmentForm() {
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [technician, setTechnician] = useState('');
    const [reason, setReason] = useState('');
    const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
        const fetchTechnicians = async () => {
            try {
                const response = await fetch('http://localhost:8080/api/technicians/'); 
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                const data = await response.json();
                if (data.technicians && Array.isArray(data.technicians)) {
                    setTechnicians(data.technicians);
                } else {
                    console.error('Technicians data is not an array:', data);
                }
            } catch (error) {
                console.error('Error fetching technicians:', error);
            }
        };
    
        fetchTechnicians();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const appointmentData = {
            vin,
            customer,
            date_time: `${date}T${time}`,
            technician,
            reason
        };

        console.log(appointmentData);

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(appointmentData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            console.log(newAppointment);
            setVin('');
            setCustomer('');
            setDate('');
            setTime('');
            setTechnician('');
            setReason('');
        }
    };

return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a service appointment</h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            className="form-control"
                            id="vin"
                            value={vin}
                            onChange={e => setVin(e.target.value)}
                            placeholder="Automobile VIN"
                            required
                        />
                        <label htmlFor="vin">Automobile VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            className="form-control"
                            id="customer"
                            value={customer}
                            onChange={e => setCustomer(e.target.value)}
                            placeholder="Customer"
                            required
                        />
                        <label htmlFor="customer">Customer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="date"
                            className="form-control"
                            id="date"
                            value={date}
                            onChange={e => setDate(e.target.value)}
                            required
                        />
                        <label htmlFor="date">Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="time"
                            className="form-control"
                            id="time"
                            value={time}
                            onChange={e => setTime(e.target.value)}
                            required
                        />
                        <label htmlFor="time">Time</label>
                    </div>
                    <div className="form-floating mb-3">
                        <select
                            className="form-select"
                            id="technician"
                            value={technician}
                            onChange={e => setTechnician(e.target.value)}
                            required
                        >
                            <option value="">Choose a technician</option>
                            {technicians.map(t => (
                                <option key={t.id} value={t.employee_id}>{t.first_name} {t.last_name}</option>
                            ))}
                        </select>
                        <label htmlFor="technician">Technician</label>
                    </div>
                    <div className="form-floating mb-3">
                        <textarea
                            className="form-control"
                            id="reason"
                            value={reason}
                            onChange={e => setReason(e.target.value)}
                            placeholder="Reason"
                            required
                        ></textarea>
                        <label htmlFor="reason">Reason</label>
                    </div>
                    <button type="submit" className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
);
}

export default CreateAppointmentForm;