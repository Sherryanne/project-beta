import React, {useEffect, useState} from 'react';

function SalesPersonHistory() {
   const [salespeople, setSalespeople] = useState([]);
   const [sales, setSales] = useState([]);
   const [filteredSales, setFilteredSales] = useState([]);




    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('http://localhost:8090/api/salespeople/');
                if (response.ok) {
                    const { salespeople } = await response.json();
                    setSalespeople(salespeople);
                }
            } catch (error) {
                console.error("Error fetching data:", error);
            }
            try {
              const salesresponse = await fetch('http://localhost:8090/api/sales/');
              if (salesresponse.ok) {
                  const info = await salesresponse.json();
                  setSales(info.sales);
              }
          } catch (error) {
              console.error("Error fetching data:", error);
          }
        };

        fetchData();
    }, []);

    const handleChange = (e) => {
      e.preventDefault()
      const filteredArray = []
      const selectedEmployeeID = e.target.value


      for (let i = 0; i < sales.length; i++) {
        const individualSale = sales[i];
        if (+individualSale.sales_person.employee_id === +selectedEmployeeID) {
          filteredArray.push(individualSale)
        }
      }
      setFilteredSales(filteredArray);
}


    return (
<><h1>Salesperson History</h1>

        <div>
          <label>
          Salesperson
          <select onChange={handleChange}>
            <option value="">Salesperson</option>
            {salespeople.map((salesperson) => (
              <option key={salesperson.id} value={salesperson.employee_id}>
                {`${salesperson.first_name} ${salesperson.last_name}`}
              </option>
            ))}
          </select>
          <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson Employee ID</th>
              <th>SalesPerson Name</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {filteredSales.map((sale) => {
              return(
              <tr key={sale.id}>
                <td>{sale.sales_person.id}</td>
                <td>{`${sale.sales_person.first_name} ${sale.sales_person.last_name}`}</td>
                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.sale_price}</td>
              </tr>
              )
            })}
          </tbody>
          </table>
        </label>

        </div>

</>
)




}
export default SalesPersonHistory;
