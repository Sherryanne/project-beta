import React, {useEffect, useState } from 'react';

function Customers() {
    const[customers, setCustomers] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('http://localhost:8090/api/customers/');
                if (response.ok) {
                    const { customers } = await response.json();
                    setCustomers(customers);
                }
            } catch (error) {
                console.error("Error fetching data:", error);
        }

    };
    fetchData();
}, []);


return (
    <><h1>Customers</h1>
<table className="table table-striped">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
        </tr>
    </thead>
    <tbody>
        {customers.map(customer=> {
            return(
                <tr key={customer.id}>
                    <td> {customer.first_name}</td>
                    <td> {customer.last_name}</td>
                    <td> {customer.phone_number}</td>
                    <td> {customer.address}</td>
                </tr>
            )
        })}


    </tbody>
</table></>
)
}
export default Customers;
