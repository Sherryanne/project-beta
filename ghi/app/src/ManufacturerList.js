import React from "react";
import {useState, useEffect} from "react";

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        try {
            const response = await fetch("http://localhost:8100/api/manufacturers");
            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);
            } else {
                console.error('HTTP Error:', response.statusText);
            }
        } catch (error) {
            console.error('Network Error:', error.message);
        }
    };

    useEffect(() => {
        fetchData();
    }, []); 

    return(
        <div>
            <h1>Manufacturers</h1>
            <table className = "table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default ManufacturerList;
