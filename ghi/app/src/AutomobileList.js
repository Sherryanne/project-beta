import React, { useState, useEffect } from 'react';

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  const fetchAutomobiles = async () => {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  const handleDelete = async (vin) => {
    const response = await fetch(`http://localhost:8100/api/automobiles/${vin}/`, {
      method: 'DELETE',
    });

    if (response.ok) {

      setAutomobiles(automobiles.filter((auto) => auto.vin !== vin));
    }
  };

  return (
    <div className='container'>
      <h1>Automobiles List</h1>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Color</th>
            <th>Year</th>
            <th>VIN</th>
            <th>Sold</th>
            <th>Model ID</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((auto) => (
            <tr key={auto.vin}>
              <td>{auto.color}</td>
              <td>{auto.year}</td>
              <td>{auto.vin}</td>
              <td>{auto.sold ? 'Yes' : 'No'}</td>
              <td>{auto.model.id}</td>
              <td>
                <button onClick={() => handleDelete(auto.vin)} className='btn btn-danger'>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobileList;