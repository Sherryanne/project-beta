from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import AutomobileVO, Salesperson, Customer, Sale
from .encoders import (
    AutomobileVOEncoder,
    SalesPersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder = SalesPersonEncoder,
            safe = False,
        )
    else: #POST
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe = False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a salesperson"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_salesperson(request, id):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                {"message": "Salesperson deleted"},
                safe = False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message":  "Salesperson does not exist"}
            )
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerEncoder,
        )
    else: #POST
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder = CustomerEncoder,
                safe = False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"}
            )
        response.status_code = 404
        return response


@require_http_methods(["DELETE"])
def api_customer(request, id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe = False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"}
            )
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({"sales": sales}, encoder = SaleEncoder, safe = False)


    else: #POST
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]["automobile_vin"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
            automobile.sold =True

            sales_person_id = content["sales_person"]
            sales_person = Salesperson.objects.get(pk=sales_person_id)
            content["sales_person"] = sales_person

            customer_id = content['customer']
            customer = Customer.objects.get(pk=customer_id)
            content["customer"] = customer

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid vin"},
                status=400,
            )

        except Salesperson.DoesNotExist:
            sales_person = None
            return JsonResponse(
                {"message": "Invalid employee id"},
                status = 400,
            )

        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status = 400,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(sale,
                            SaleEncoder,
                            safe=False)


@require_http_methods(["DELETE"])
def api_sale(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe = False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Sale does not exist"}
            )
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def api_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder = AutomobileVOEncoder,
            safe = False,
        )
