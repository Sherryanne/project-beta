# CarCar

Team:

* Cory Egan - Auto Sales
* Sherryanne Shen - Auto Services



Explicit instructions to run this project

1. Make sure you have Docker, GitLab, and Node.js 18.2 or above on your computer.

2. Fork this repository.

3. Build and run the project using Docker with these commands:

docker volume create beta-data
docker-compose build
docker-compose up

4. After running these commands make sure all of your Docker containers are running.

5. Next you can view the project in the browser at localhost:3000!



Here is a diagram of the project:

![Alt text](image.png)



Design overview
There are three main components on the Backend of this project that create RESTful Django APIs.  The Inventory API, Sales API and Services API are all different microservices that come together to render on a dynamic React front-end.  The poller is connected with the Inventory API model Automobile.  Automobile Value Object models are created in Sales API and Services API to integrate the data from the Automobile model.

URLS, Ports and Interactions:
    URLs and Ports to access your microservice through your local host and Insomnia.
    Inventory API: http://localhost:8100
    Sales API: http://localhost:8090
    Services API: http://localhost:8080

Specific Requests:
    This will cover how to enter your URL into Insomnia.  Also underneath are explicit examples of how to format your JSON body to send data*:
    *Note, JSON body: is not to be used in your Insomnia.  Just starting after JSON body: is what should go into your HTTP requests.

1. Inventory API
    Manufacturers
        List manufacturers	            GET	    http://localhost:8100/api/manufacturers/

        Create a manufacturer	        POST	http://localhost:8100/api/manufacturers/
        JSON body:

            {
                "name": "Chrysler"
            }

        Get a specific manufacturer	    GET	    http://localhost:8100/api/manufacturers/:id/
        Update a specific manufacturer	PUT	    http://localhost:8100/api/manufacturers/:id/
        Delete a specific manufacturer	DELETE	http://localhost:8100/api/manufacturers/:id/


    Vehicle Models
        List vehicle models	                    GET	    http://localhost:8100/api/models/
        Create a vehicle model	                POST	http://localhost:8100/api/models/
        JSON body:

            {
                "name": "Sebring",
                "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
                "manufacturer_id": 1
            }

        Get a specific vehicle model	        GET	    http://localhost:8100/api/models/:id/
        Update a specific vehicle model	        PUT	    http://localhost:8100/api/models/:id/
        Delete a specific vehicle model	        DELETE	http://localhost:8100/api/models/:id/


    Automobile Information
        List automobiles	            GET	    http://localhost:8100/api/automobiles/
        Create an automobile	        POST	http://localhost:8100/api/automobiles/
        JSON body:

            {
                "color": "red",
                "year": 2012,
                "vin": "1C3CC5FB2AN120174",
                "model_id": 1
            }
        Get a specific automobile	    GET	    http://localhost:8100/api/automobiles/:vin/
        Update a specific automobile	PUT	    http://localhost:8100/api/automobiles/:vin/
        Delete a specific automobile    DELETE	http://localhost:8100/api/automobiles/:vin/


2. Sales API
    Salespeople
        List salespeople	            GET 	http://localhost:8090/api/salespeople/
            JSON body:
            "salespeople": [
            {
                "id": 1,
                "first_name": "Clark",
                "last_name": "Griswold",
                "employee_id": 1
            }]

        Create a salesperson	        POST	http://localhost:8090/api/salespeople/
            JSON body:

            {
                "first_name": "Clark",
                "last_name": "Griswold",
                "employee_id": 1
            }

        Delete a specific salesperson	DELETE	http://localhost:8090/api/salespeople/:id/
            Here you will put 1 that corresponds to the id in the HTTP request.
            JSON body:

            "salespeople": [
            {
                "id": 1,
                "first_name": "Clark",
                "last_name": "Griswold",
                "employee_id": 1
            }]

    Customers
        List customers	            GET	    http://localhost:8090/api/customers/
            JSON body:

                "customers": [
            {
                "href": "/api/customer/1/",
                "id": 1,
                "first_name": "Santa",
                "last_name": "Claus",
                "address": "North Pole",
                "phone_number": "111-1111"
            }]

        Create a customer	        POST	http://localhost:8090/api/customers/
            JSON body:

            {
            "first_name": "Santa",
            "last_name": "Claus",
            "address": "North Pole",
            "phone_number": "111-1111"
            }

        Delete a specific customer	DELETE	http://localhost:8090/api/customers/:id/
            Here you will put 1 that corresponds to the id in the HTTP request.
            JSON body:

            "customers": [
            {
                "href": "/api/customer/1/",
                "id": 1,
                "first_name": "Santa",
                "last_name": "Claus",
                "address": "North Pole",
                "phone_number": "111-1111"
            }]

    Sales
        List sales	    GET	    http://localhost:8090/api/sales/
            JSON body:
                                            {
                                                "sales": [
                                                        {
                                                            "href": "/api/sales/1",
                                                            "id": 1,
                                                            "automobile": {
                                                                "id": 1,
                                                                "vin": "1C3CC5FB2AN120174",
                                                                "import_href": null,
                                                                "sold": false
                                                            },
                                                            "sales_person": {
                                                                "id": 1,
                                                                "first_name": "Clark",
                                                                "last_name": "Griswold",
                                                                "employee_id": 1
                                                            },
                                                            "customer": {
                                                                "href": "/api/customer/1/",
                                                                "id": 1,
                                                                "first_name": "Santa",
                                                                "last_name": "Claus",
                                                                "address": "North Pole",
                                                                "phone_number": "111-1111"
                                                            },
                                                            "sale_price": "1000000.00"
                                                        }
                                                        ]
                                            }
        Create a sale	POST	http://localhost:8090/api/sales/
            JSON body:

            {
                    "automobile":
                        {
                        "sold": false,
                        "automobile_vin": "1C3CC5FB2AN120174"},
                "sales_person": 1,
                "customer": 1,
                "sale_price": "1000000"
            }

        Delete a sale	DELETE	http://localhost:8090/api/sales/:id
            Here you will put 1 that corresponds to the sales id in the HTTP request.
            JSON body:

                                            {
                                                "sales": [
                                                        {
                                                            "href": "/api/sales/1",
                                                            "id": 1,
                                                            "automobile": {
                                                                "id": 1,
                                                                "vin": "1C3CC5FB2AN120174",
                                                                "import_href": null,
                                                                "sold": false
                                                            },
                                                            "sales_person": {
                                                                "id": 1,
                                                                "first_name": "Clark",
                                                                "last_name": "Griswold",
                                                                "employee_id": 1
                                                            },
                                                            "customer": {
                                                                "href": "/api/customer/1/",
                                                                "id": 1,
                                                                "first_name": "Santa",
                                                                "last_name": "Claus",
                                                                "address": "North Pole",
                                                                "phone_number": "111-1111"
                                                            },
                                                            "sale_price": "1000000.00"
                                                        }
                                                        ]
                                            }

3. Service API
    Technicians
        List technicians	            GET 	http://localhost:8080/api/technicians/
            JSON Body:
                {
	                "technicians": [
		                {
			                "id": 2,
			                "employee_id": "2",
			                "first_name": "Anne",
			                "last_name": "Li"
		                },
		                {
			                "id": 5,
			                "employee_id": "4",
			                "first_name": "Sherryanne",
			                "last_name": "Shen"
		                },
		                {
			                "id": 16,
			                "employee_id": "1",
                            "first_name": "Joshua",
			                "last_name": "Park"
		                }
	                ]
                }
        Create a technician	            POST	http://localhost:8080/api/technicians/
            JSON Body:
                {
	                "employee_id": "22",
                    "first_name": "Sherry",
                    "last_name":"Anne"
                }
        Delete a specific technician	DELETE	http://localhost:8080/api/technicians/:id/


    Appointments
        List appointments	                        GET	    http://localhost:8080/api/appointments/
            JSON Body:
                {
	                "appointments": [
		            {
			            "id": 4,
			            "customer": "Anne",
			            "vin": "555",
                        "reason": "Fixing",
                        "date_time": "2023-12-20T18:28:00+00:00",
                        "technician": {
                            "id": 2,
                            "employee_id": "2",
                            "first_name": "Anne",
                            "last_name": "Li"
                        },
                        "status": "pending"
                    },
                    {
                        "id": 9,
                        "customer": "Kesen",
                        "vin": "222",
                        "reason": "Fixing",
                        "date_time": "2023-12-21T13:58:00+00:00",
                        "technician": {
                            "id": 5,
                            "employee_id": "4",
                            "first_name": "Sherryanne",
                            "last_name": "Shen"
                        },
                        "status": "finished"
                    }
                ]
            }
        Create an appointment	                    POST	http://localhost:8080/api/appointments/
            JSON Body:
                {
                    "customer": "Joshua",
                    "vin": "4",
                    "reason": "Fixing the car",
                    "technician": "2",
                    "status": "pending",
                    "date_time": "2023-12-20T10:00:00"
                }

        Delete an appointment	                    DELETE	http://localhost:8080/api/appointments/:id/
        Set appointment status to "canceled"	    PUT	    http://localhost:8080/api/appointments/:id/cancel/
        Set appointment status to "finished"	    PUT	    http://localhost:8080/api/appointments/:id/finish/



## Sales microservice

Models:

Customer Model
    -Contains name, address and phone_number properties.  The phone_number property is set to unique = True so no duplicate phone_numbers.
Salesperson Model
    -Contains first_name, last_name, and employee_id.  The employee_id property is set to unique = True so no duplicate employee_id's.
Sale Model
    -Contains automobile foreign key which relates to AutomobileVO.  Explained in detail below.  A sale_price property.  Also contains two other foreign keys, sales_person which relates to Salesperson and customer which relates to the Customer model.

    -Special Value Object
AutomobileVO Model
    - Contains vin, import_href and sold properties.  This model is connected with the poll automobile data from the inventory-api.  If you look at your docker containers it is running at sales-poller-1.  It allows the data (vin, import_href, sold) of the AutomobileVO to be realized in the sales-api microservice without modifying the inventory-api.  The vin and import_href properties are both unique=True so they cannot be duplicated.


## Service microservice

Models:

AutomobileVO Model
- Description: Represents a vehicle with a unique Vehicle Identification Number (VIN).
- Fields: "Vin" is a string field with a maximum length of 100 characters. It is unique, ensuring no duplicate VINs. "sold" is a boolean field indicating whether the automobile is sold (default is False).

Technician Model
- Description: Represents a technician or employee within the system.
- Fields: "first_name" is a string field to store the technician's first name. "last_name" is a string field to store the technician's last name.
"employee_id" is a unique string field for the technician's employee ID.

Appointment Model
- Description: Details an appointment scheduled for service or maintenance.
- Fields: "Vin" is a string field storing the VIN of the automobile for the appointment. "Customer" is a string field for the customer's name or identifier. "date_time" is a DateTime field for scheduling the appointment. "Reason" is a string field detailing the reason for the appointment. "Technician" is a foreign key linking to the Technician model. "Status" is a string field indicating the current status of the appointment.
